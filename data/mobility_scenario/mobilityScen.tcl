#Mobility Patterns Generator for ns-2 simulator
#Based on the Community Based Mobility Model
#Copyright (C) Mirco Musolesi University College London
#              m.musolesi@cs.ucl.ac.uk
#Version 0.1 May 2006
set god_ [God instance]
$node_(0) set X_ 311.469776
$node_(0) set Y_ 3.862267
$node_(0) set Z_ 0.000000
$node_(1) set X_ 374.732531
$node_(1) set Y_ 118.244031
$node_(1) set Z_ 0.000000
$ns_ at 0.000000 "$node_(0) setdest 503.154147 785.648972 5.000656"
$ns_ at 0.000000 "$node_(1) setdest 482.102677 421.407210 4.999856"
$ns_ at 60.000000 "$node_(1) setdest 928.329564 326.465757 4.999856"
$ns_ at 150.000000 "$node_(1) setdest 201.258688 932.421436 4.999856"
$ns_ at 156.000000 "$node_(0) setdest 397.713631 508.734482 5.000656"
$ns_ at 212.000000 "$node_(0) setdest 156.378975 624.515346 5.000656"
$ns_ at 262.000000 "$node_(0) setdest 904.499567 13.837539 5.000656"
$ns_ at 295.000000 "$node_(1) setdest 702.043606 668.208864 4.999856"
$ns_ at 395.000000 "$node_(1) setdest 939.474973 791.708019 4.999856"
$ns_ at 412.000000 "$node_(0) setdest 273.607984 167.715131 5.000656"
$ns_ at 443.000000 "$node_(1) setdest 928.985546 335.842739 4.999856"
$ns_ at 534.000000 "$node_(1) setdest 689.586023 30.803023 4.999856"
$ns_ at 538.000000 "$node_(0) setdest 365.403473 675.168286 5.000656"
$ns_ at 596.000000 "$node_(1) setdest 926.145130 23.923988 4.999856"
$ns_ at 641.000000 "$node_(0) setdest 936.030227 339.141614 5.000656"
$ns_ at 644.000000 "$node_(1) setdest 927.419801 349.856429 4.999856"
$ns_ at 709.000000 "$node_(1) setdest 829.787409 94.712931 4.999856"
$ns_ at 756.000000 "$node_(0) setdest 973.448326 67.665410 5.000656"
$ns_ at 760.000000 "$node_(1) setdest 934.859939 982.265952 4.999856"
