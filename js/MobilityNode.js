var nodeMaterial = new THREE.MeshBasicMaterial({
    color: 0xffff00,
    transparent: false
});

var nodeAreaMaterial = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    transparent: true,
    opacity: 0.1
    });

var selectedNodeMaterial = new THREE.MeshBasicMaterial({
    color: 0xff00ff,
    transparent: true,
    opacity: 0.4
});

var MobilityNode = ( function () {
    var data = {};

    data.init = function(new_id, startx, starty, startz) {
        data.id = new_id;
        data.start_pos = new THREE.Vector3(startx, starty, startz);

        data.destinations = {};

        data.directon_vec;


        data.previous_destination = data.start_pos.clone();

        data.next_destination;

        data.position = data.start_pos.clone();



        var node_geometry = new THREE.SphereGeometry( 2 );
        data.node_mesh = new THREE.Mesh( node_geometry, nodeMaterial );
        data.node_mesh.name = data.id.toString();

        data.node_mesh.position.x = data.position.x;
        data.node_mesh.position.y = 0.0;
        data.node_mesh.position.z = data.position.z;


        var node_area_geometry = new THREE.SphereGeometry( 250 );
        data.node_area_mesh = new THREE.Mesh( node_area_geometry, nodeAreaMaterial );
        data.node_area_mesh.name = data.id.toString();

        data.node_area_mesh.position.x = data.position.x;
        data.node_area_mesh.position.y = 0.0;
        data.node_area_mesh.position.z = data.position.z;

        nodesGroup.add(data.node_mesh);
        nodesGroup.add(data.node_area_mesh);

        targets.push(data.node_area_mesh);

        return data;

    };

    data.get_next_destination = function(current_node) {
        var min_frame = 0;
        var changed = false;
        for (var key in current_node.destinations) {
            if (current_node.destinations.hasOwnProperty(key)) {
                var frame = parseFloat(key);
                if (frame < current_time){
                    continue;
                }
                else if (frame > current_time){
                    if (min_frame === 0){
                        min_frame = frame;
                        changed = true;
                        continue;
                    }
                    if (frame < min_frame) {
                        changed = true;
                        min_frame = frame;
                    }
                }
            }
        }

        if ( changed ) {
            return current_node.destinations[min_frame];
        }
        else return undefined;
    };

    data.add_destination = function(newTime, newX, newY, newZ) {
        data.destinations[newTime] = new THREE.Vector3(newX, newY, newZ)
    };

    data.calculate_path_vector = function(current_node){
        var vec = new THREE.Vector3(0,0,0);

        //data.get_next_destination(current_node);
        if (current_node.previous_destination instanceof THREE.Vector3)
            vec.subVectors(current_node.next_destination.destination, current_node.previous_destination);
        else
            vec.subVectors(current_node.next_destination.destination, current_node.previous_destination.destination);
        vec.normalize();

        current_node.directon_vec = vec.clone();

    };

    data.update = function(current_node) {

        var destination;

        if (! clock.running )
             return;

        if (last_calculation >= 0.95 || nodes_need_update){
            destination = this.get_next_destination(current_node);

            if (destination){
                if (destination != current_node.next_destination){
                    if (current_node.next_destination)
                        current_node.previous_destination  = current_node.next_destination;
                    current_node.next_destination = destination;


                    current_node.calculate_path_vector(current_node);
                }
            }
            //nodes_need_update = false;
        }
        else {
            destination = current_node.next_destination;
        }




        current_node.last_frame = clock.getDelta();

        if (!current_node.directon_vec){
            current_node.calculate_path_vector(current_node);
        }


         var movementVec = current_node.directon_vec.clone().normalize();

         //movementVec.multiplyScalar(current_node.speed*last_delta);
         movementVec.multiplyScalar(current_node.next_destination.speed*last_delta*playback_speed);



         if ( clock.running ) {

             current_node.position.add(movementVec);
         }



        //current_node.render();

    };

    data.project_on_terrain = function(current_node, terrain) {
        if (current_node.node_mesh) {
            var rayStart = new THREE.Vector3(current_node.position.x, current_node.position.y, current_node.position.z);

            var rayDirection = new THREE.Vector3(0, 1, 0).normalize();

            var ray = new THREE.Ray(rayStart, rayDirection);

            var faces =  terrain.children[0].geometry.faces;

            for (var i=0; i < faces.length; ++i) {
                var new_position = ray.intersectTriangle(faces[i].triangle_plane.a, faces[i].triangle_plane.b, faces[i].triangle_plane.c, false, null);

                if (new_position) {
                    face_id = i;

                    this.set_node_position(current_node, new_position);

                    //var plane = current_node.

                    //current_node.directon_vec =

                    break;
                }

            }

            current_node.current_face  = face_id;

        }
    };

    data.set_node_position = function(current_node, new_position) {

        current_node.current_pos.x = new_position.x;
        current_node.current_pos.y = new_position.y+y_offset;
        current_node.current_pos.z = new_position.z;

        current_node.node_mesh.position.x = new_position.x;
        current_node.node_mesh.position.y = new_position.y+y_offset;
        current_node.node_mesh.position.z = new_position.z;

        current_node.node_area_mesh.position.x = new_position.x;
        current_node.node_area_mesh.position.y = new_position.y+y_offset;
        current_node.node_area_mesh.position.z = new_position.z;

    };

    data.render = function(current_node){
        if (current_node.node_mesh){

            var rayStart = new THREE.Vector3(current_node.position.x, current_node.position.y, current_node.position.z);

            var rayDirection = new THREE.Vector3(0, 1, 0).normalize();

            var ray = new THREE.Ray(rayStart, rayDirection);

            var faces =  terrain.children[0].geometry.faces;
            var vertices =  terrain.children[0].geometry.vertices;

            //console.log(faces);
            var min_distance;
            var face_id = -1;

            for (var i=0; i < faces.length; ++i){
                var node_point = new THREE.Vector2(current_node.position.x, current_node.position.z);
                var face_mid_2d = new THREE.Vector2(faces[i].midPoint.x, faces[i].midPoint.z);

                //var distance = node_point.distanceTo(face_mid_2d);

                //if (! min_distance ) {
                //    min_distance = distance;
                //    face_id = i;
                //}
                //else if (min_distance < distance){
                //    min_distance = distance;
                //    face_id = i;
                //}

                var new_position = ray.intersectTriangle(vertices[faces[i].a], vertices[faces[i].b], vertices[faces[i].c], false, null);

                if (new_position) {
                    //console.log(new_position);
                    face_id = i;
                    current_node.node_mesh.position.x = new_position.x;
                    current_node.node_mesh.position.y = new_position.y+2;
                    current_node.node_mesh.position.z = new_position.z;
                    break;
                }

            }

            if (face_id !== -1) {

                current_node.current_face = face_id;

            }
            else {
                current_node.current_face = undefined;
            }

        }

    };


    return data;

})();