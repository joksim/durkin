var GUI =
{
    ms_SmoothShading: false,
    ms_Parameters: {},


    Initialize: function( inParameters )
    {
        gui = new dat.GUI();
        this.ms_Parameters = inParameters;
        guiParameters =
        {
            /*
            width: inParameters.width,
            height: inParameters.height,
            widthSegments: inParameters.widthSegments,
            heightSegments: inParameters.heightSegments,
            depth: inParameters.depth,
            param: inParameters.param,
            filterparam: inParameters.filterparam,
            /*
             alea: GENERATORS.Random.MersenneTwister,
             generator: GENERATORS.Generator.PerlinNoise,
             colors: GENERATORS.ms_Colors.indexOf( inParameters.postgen[0] ),
             filter: GENERATORS.ms_Filters.indexOf( inParameters.filter[0] ),
             effect: GENERATORS.ms_Effects.indexOf( inParameters.effect[0] ),
             */
            //heightMap: false,
            //smoothShading: false,

            //github: function() {},
            geometryFile : function() { $('#geometryFile').click(); },
            mobilityFile : function() { $('#mobilityFile').click(); },
            //update: function() { GUI.Update(); }
        };

        var files_folder = gui.addFolder('Load Files');
        files_folder.add( guiParameters, 'geometryFile').name('Load Geometry File (TIN ONLY)');//.onChange( function( inValue ) {
        //GUI.ms_Parameters.tinFile = inValue;
        //} );
        files_folder.add( guiParameters, 'mobilityFile').name('Load Mobility File (TCL ONLY)');
        files_folder.open();
        /*
         var generatorFolder = gui.addFolder('Generator');
         generatorFolder.add( guiParameters, 'alea', GENERATORS.Random ).name('Random').onChange( function( inValue ) {
         GUI.ms_Parameters.alea = GENERATORS.ms_Randoms[inValue];
         } );
         generatorFolder.add( guiParameters, 'generator', GENERATORS.Generator ).name('Noise').onChange( function( inValue ) {
         GUI.ms_Parameters.generator = GENERATORS.ms_Generators[inValue];
         } );
         generatorFolder.add( guiParameters, 'param' ).min(1.1).max(10).step(0.1).name('Parameter').onChange( function( inValue ) {
         GUI.ms_Parameters.param = inValue;
         } );
         generatorFolder.add( guiParameters, 'filter', GENERATORS.Filter ).name('Filter').onChange( function( inValue ) {
         GUI.ms_Parameters.filter = ( inValue == 0 )? [] : [ GENERATORS.ms_Filters[inValue] ];
         } );
         generatorFolder.add( guiParameters, 'filterparam' ).min(0).max(10).step(0.1).name('Filter param').onChange( function( inValue ) {
         GUI.ms_Parameters.filterparam = inValue;
         } );
         generatorFolder.add( guiParameters, 'effect', GENERATORS.Effect ).name('Effect').onChange( function( inValue ) {
         GUI.ms_Parameters.effect = ( inValue == 0 )? [] : [ GENERATORS.ms_Effects[inValue] ];
         } );
         generatorFolder.add( guiParameters, 'colors', GENERATORS.PostGen ).name('Colors').onChange( function( inValue ) {
         GUI.ms_Parameters.postgen = ( inValue == 0 )? [] : [ GENERATORS.ms_Colors[inValue] ];
         } );
         generatorFolder.open();
         */

        //var otherFolder = gui.addFolder('Other');
        /*
         otherFolder.add( guiParameters, 'heightMap' ).name('Height map').onChange( function( inValue ) {
         if( inValue )
         $('#heightmap').show();
         else
         $('#heightmap').hide();
         } );
         */
        /*
        otherFolder.add( guiParameters, 'smoothShading' ).name('Smooth shading').onChange( function( inValue ) {
            GUI.ms_SmoothShading = inValue;
        } );

        */
        //otherFolder.add( guiParameters, 'github' ).name('<a href="https://github.com/jbouny/terrain-generator" target="_blank">GitHub</a>');
        //otherFolder.open();

        //gui.add( guiParameters, 'update' ).name('<b>Update</b>');
    },

    Update: function()
    {
        this.ms_Parameters.geometryFile = $('#geometryFile')[0].files[0];
        this.ms_Parameters.mobilityFile = $('#mobilityFile')[0].files[0];
        TERRAINGENDEMO.Load( this.ms_Parameters );
    }
};