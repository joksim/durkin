/**
 * Created by joksim
 */

MSLoader = function (data) {
        nodes = {};

        var mobilityNode_definition = /\$node_\(\d+\) set [XYZ]_ \d+\.\d+/i;

        var mobilityNode_destination = /\$ns_ at \d+.\d+ "\$node_\(\d+\) setdest \d+.\d+ \d+.\d+ \d+.\d+"/;

        var lines = data.split( "\n" );

        var skip = 1;

        var node_id, node_x, node_y, node_z;
        node_id = node_x = node_y = node_z = null;

        for ( var i = 0; i < lines.length;) {
            //node_id = node_x = node_y = node_z = undefined;
            var line = lines[ i ];
            line = line.trim();

            var result;

            if ( line.length === 0 || line.charAt( 0 ) === '#' ) {
                i++;
                continue;
            }
            else if ( ( result = line.match(mobilityNode_definition) !== null )) {

                node_id = parseInt(line.match(/\d+/)[ 0 ]);

                var tmp = null;
                if (line.match(/set X_/)){
                    //console.log("Node: id = " + node_id);
                    tmp = line.match(/set X_ \d+\.\d+/)[0];
                    node_x = parseFloat(tmp.slice(7));
                }
                else if (line.match(/set Y_/)) {
                    tmp = line.match(/set Y_ \d+\.\d+/)[0];
                    node_y = parseFloat(tmp.slice(7));
                }
                else if (line.match(/set Z_/)){
                    tmp = line.match(/set Z_ \d+\.\d+/)[0];
                    node_z = parseFloat(tmp.slice(7));

                    // The axis are switched, so y <=> z
                    var node = MobilityNode.init(node_id, node_x, node_z, node_y);

                    nodes[node_id.toString()] = $.extend(true, {}, node);
                }

                i++;

            }

            else if (result = line.match(mobilityNode_destination) !== null) {

                // Parsing timestamp
                tmp = line.match(/\$ns_ at \d+\.\d+/)[0];
                var time  = parseFloat(tmp.slice(8));

                // Parsing node_id
                tmp = line.match(/\$node_\(\d+\) setdest/)[0];
                var current_node_id = tmp.slice(7).split(')')[0];


                // Parsing coordinates and speed
                tmp = line.match(/setdest \d+.\d+ \d+.\d+ \d+.\d+/)[0].split(' ');

                var x = parseFloat(tmp[1]);
                var z = parseFloat(tmp[2]);
                var speed = parseFloat(tmp[3]);

                var vec = new THREE.Vector3(x, 0.0, z);

                //destination = {};

                //destination['time'] = time;
                //destination['position'] = vec;

                //nodes[current_node_id].destinations = {};
                if (time === 0){
                    nodes[current_node_id].start_pos = vec.clone();
                    nodes[current_node_id].current_pos = vec.clone();
                }
                else {
                    //TODO: Make it work
                    nodes[current_node_id].destinations[time] = {
                        speed: speed,
                        destination: vec.clone()
                    };
                }
                //console.log(destination);
                i++;
            }

            else {
                console.log( "[THREE.MSLoader] Unhandled line: " + line );
                i++;

            }

        }

    window.NODES = $.extend(true, {}, nodes);

    updateMobilityScenario();

    };